import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'login.dart';
void main() {

  runApp (DevicePreview(
    enabled: true,
    builder: (BuildContext context) => MyApp(),
  ));
}
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginLayout()
    );
  }
}
class HomePage2 extends StatelessWidget {
  const HomePage2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: MyStatefulWidget(),

    );
  }
}


class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({super.key});

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;
  final ScrollController _homeController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.brown,
        title: const Text('BURAPHA UNIVERSITY'),
      ),
      body: _listViewBody(),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.domain),
            label: 'หน้าหลัก',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.note),
            label: 'ลงทะเบียนเรียน',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amberAccent[800],
        onTap: (int index) {
          switch (index) {
            case 0:
            // only scroll to top when current index is selected.
              if (_selectedIndex == index) {
                _homeController.animateTo(
                  0.0,
                  duration: const Duration(milliseconds: 500),
                  curve: Curves.easeOut,
                );
              }
              break;
            case 1:
              showModal(context);
              break;
          }
          setState(
                () {
              _selectedIndex = index;
            },
          );
        },
      ),
    );
  }

  void showModal(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        content: const Text('คุณได้ทำการลงทะเบียนสำเร็จ'),
        actions: <TextButton>[
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text('Close'),
          )
        ],
      ),
    );
  }
}

Widget _listViewBody() {
  return Scaffold(
    body: Container(
      padding: EdgeInsets.symmetric(vertical: 10.0,horizontal: 10.0),
      // height: MediaQuery.of(context).size.height,
      color: Colors.brown.shade200,
      child: Column(
        children: <Widget>[
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                getExpanded('ค่าใช้จ่าย','ภาระค่าใช้จ่ายทุน'),
                getExpanded('ยื่นคำร้อง','ยื่นคำร้อง'),
              ],
            ),
          ),
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                getExpanded('ประวัติส่วนตัว','ประวัติส่วนตัว'),
                getExpanded('ผลการเรียน','ผลการเรียน'),
              ],
            ),
          ),
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                getExpanded('ปฏิทิน','ปฏิทินการศึกษา'),
                getExpanded('ออกจากระบบ','ออกจากระบบ'),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}

Expanded getExpanded(String image, String mainText){
  return Expanded(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    'images/$image.png',
                    height: 120.0,
                  ),
                  Text(
                    mainText,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20
                    ),
                  ),
                ],
              ),
              margin: EdgeInsets.only(left: 10.0,top: 10.0,right: 10.0,bottom: 10.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5),
                  topRight: Radius.circular(5),
                  bottomLeft: Radius.circular(5),
                  bottomRight: Radius.circular(5),
                ),
                boxShadow: [
                  BoxShadow(),
                ],

              ),

            ),

          ),
        ],
      )
  );
}
