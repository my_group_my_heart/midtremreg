import 'package:flutter/material.dart';

import 'package:regmidteam/main.dart';

void main() {
  runApp(LoginLayout());
}
class LoginLayout extends StatelessWidget {
  double rowPadding = 24.0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.white,
        body: Container(

        decoration: const BoxDecoration(
        image: DecorationImage(
        image: AssetImage("images/เข้าสู่ระบบ.png"),
        fit: BoxFit.cover,
      ),
    ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[


              Center(
                child: ConstrainedBox(
                  constraints: const BoxConstraints(maxWidth: 300.0),
                  child: Column(
                    children: [
                      const SizedBox(height: 80.0),
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 10.0),
                        child: Icon(
                          Icons.account_box, size: 100, color: Colors.blue,
                          // color: Colors.indigo.shade800,
                        ),
                      ),
                      const SizedBox(height: 20.0),
                      const TextField(
                          decoration: InputDecoration(
                            labelText: 'Username',
                          )),
                      const TextField(
                          decoration: InputDecoration(labelText: 'Password')),
                      const SizedBox(height: 30.0),
                      ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => const HomePage2()) );
                          },
                          child: const Text('Login')),
                    ],
                  ),
                ),
              ),
            ],
          ), /* add child content here */
        ),
      ),
    );
  }
}

